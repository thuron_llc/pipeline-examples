package pipeline.examples

class pullrequest {
    def steps
    def id

    pullrequest(steps,id) {
        this.steps = steps
        this.id = id
    }

    def comment(message) {
        steps.echo "message ${message} id ${id}"
    }
}